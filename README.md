Jeu du Devin en JavaScript

Utilisation de 3 Fonction : 
    - Une fonction qui compare la réponse utilisateur avec le nombre généré aléatoirement à faire deviner.
    - Une fonction random d'entier compris entre 0 et 500 en utilisant la fonction math.random() qui renvoit un entier entre 0 et 1 et math.floor qui renvoit l'entier le plus grand possible par rapport à son argument.
    - Une fonction principale pour faire marcher le jeu et qui sera lancé avec un bouton sur une page HTML.

